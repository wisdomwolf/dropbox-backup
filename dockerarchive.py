import click
import os
from datetime import datetime
from subprocess import call

# docker run --rm --volumes-from $argv[1] -v (pwd):/backup ubuntu tar cvf /backup/$argv[2] $argv[3]

@click.group()
def cli():
	pass


@cli.command()
@click.option('--container', prompt=True, help='Name of container to backup')
@click.option('--volume', help='Volume to backup from container')
@click.option('--directory', '-d', default=lambda: os.path.expanduser('~'), help='Location to store backup to')
@click.option('--name', default=None, help='Name of file to save backup as')
@click.option('--dryrun', is_flag=True, help='Echo command to be run without execution')
def backup(container, directory, volume, name, dryrun):
    """Backup Docker container to tar file"""
    name = name or '{}_backup_{}.tar'.format(container, datetime.today().strftime('%Y%m%d-%H%M%S'))
    cmd = f'docker run --rm --volumes-from {container} -v {directory}:/backup ubuntu tar cvf /backup/{name} /{volume}'
    if dryrun:
        click.echo(cmd)
    else:
        call(cmd, shell=True)
        #call('docker', ['run', '--rm',  '--volumes-from', container, '-v', f'{directory}:/backup', 'ubuntu', 'tar', 'cvf', f'/backup/{name}', f'/{volume}'])


@cli.command()
@click.option('--container', prompt=True)
@click.option('--volume')
@click.option('--dryrun', is_flag=True)
@click.argument('filename')
def restore(container, volume, filename, dryrun):
    """Restore volumes to Docker container from tar file"""
    if dryrun:
        click.echo(f'docker run --rm --volumes-from {container} -v {directory}:/backup ubuntu tar cvf /backup/{name} /{volume}')
    else:
        pass

#docker run --rm --volumes-from jackett -v /home/wisdomwolf:/backup ubuntu tar cvf /backup/jackett_backup_20170512.tar /config
#docker run --rm --volumes-from nodered -v (pwd):/backup ubuntu bash -c "cd /data && tar xvf /backup/nodered_backup_20170511.tar --strip 1"
