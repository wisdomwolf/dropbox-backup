from setuptools import setup

setup(
    name='docker-archive',
    version='0.1',
    py_modules=['dockerarchive'],
    install_requires=[
        'Click',
    ],
    entry_points='''
        [console_scripts]
        docker-archive=dockerarchive:cli
    ''',
)